FROM node:11.9.0
COPY . .
RUN npm install
CMD ["npm", "start"]
EXPOSE 3000